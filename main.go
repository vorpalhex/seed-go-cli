package main

import (
	"gitlab.com/vorpalhex/seed-go-cli/cmd"
)

func main() {
	cmd.Execute()
}
